import pygame
import os


class Main:
    WIDTH, HEIGHT = 900, 500

    WHITE = (255, 255, 255)
    BLACK = (0, 0, 0)
    GREEN = (0, 128, 0)
    YELLOW = (255, 255, 0)
    RED = (255, 0, 0)

    FPS = 60

    BORDER_WIDTH = 20

    def __init__(self):
        self.WIN = pygame.display.set_mode((self.WIDTH, self.HEIGHT))  # new window that is 900 by 500

        self.clock = pygame.time.Clock()
        self.previous_time = pygame.time.get_ticks()

        self.run = True
        self.space_surface = pygame.transform.scale(pygame.image.load(os.path.join('Assets', 'space.png')),
                                                    (self.WIDTH, self.HEIGHT))
        self.red = SpaceShip('Assets/spaceship_red.png', 0, 0, "wasd")
        self.yellow = SpaceShip('Assets/spaceship_yellow.png', self.WIDTH - SpaceShip.LENGTH, 0, "arrows")
        self.border = pygame.Rect((self.WIDTH - self.BORDER_WIDTH) / 2, 0, self.BORDER_WIDTH, self.HEIGHT)

    def main(self):
        while self.run:
            self.clock.tick(self.FPS)

            for event in pygame.event.get():  # show all the events happening in the game.
                if event.type == pygame.QUIT:
                    self.run = False
            self.draw_window()
        pygame.quit()

    def draw_window(self):

        self.WIN.blit(self.space_surface, (0, 0))  # draw background
        pygame.draw.rect(self.WIN, self.BLACK, self.border)  # draw border

        keys = pygame.key.get_pressed()  # check which keys were pressed

        self.red.draw(keys, self.yellow.bullets)
        self.yellow.draw(keys, self.red.bullets)

        pygame.display.update()


class Bullet:
    BULLET_LENGTH = 10
    BULLET_HEIGHT = 5

    def __init__(self, x, y, control_type):
        self.rect = pygame.Rect(x, y, self.BULLET_LENGTH, self.BULLET_HEIGHT)
        self.controlType = control_type

    def update_pos(self):
        if self.controlType == "wasd":
            self.rect.right += 10
        else:
            self.rect.left -= 10

    def draw(self):
        if self.controlType == "wasd":
            pygame.draw.rect(main.WIN, Main.RED, self.rect)
        else:
            pygame.draw.rect(main.WIN, Main.YELLOW, self.rect)


class SpaceShip:
    LENGTH = 56
    HEIGHT = 28

    def __init__(self, path, x, y, control_type):
        self.surface = pygame.transform.scale(pygame.image.load(os.path.join(path)), (self.LENGTH, self.HEIGHT))
        self.rect = self.surface.get_rect(topleft=(x, y))
        self.bullets = []
        self.MAX_BULLETS = 3
        self.control_type = control_type
        if control_type == "wasd":
            self._life = HealthBar(0, 0, 100, 10, 3)
        else:
            self._life = HealthBar(Main.WIDTH - 100, 0, 100, 10, 3)

    def _update_player_pos(self, keys):

        if self.control_type == "wasd":
            if keys[pygame.K_a] and self.rect.x - 5 >= 0:  # We can check if a key is pressed like this
                self.rect.x -= 5
            if keys[pygame.K_d] and self.rect.right + 5 <= main.border.left:
                self.rect.x += 5
            if keys[pygame.K_w] and self.rect.y - 5 >= 0:
                self.rect.y -= 5
            if keys[pygame.K_s] and self.rect.bottom + 5 <= Main.HEIGHT:
                self.rect.y += 5

        elif self.control_type == "arrows":
            if keys[pygame.K_LEFT] and self.rect.left - 5 >= main.border.right:  # Move left for arrow key control
                self.rect.x -= 5
            if keys[pygame.K_RIGHT] and self.rect.right + 5 <= Main.WIDTH:  # Move right for arrow key control
                self.rect.x += 5
            if keys[pygame.K_UP] and self.rect.top - 5 >= 0:  # Move up for arrow key control
                self.rect.y -= 5
            if keys[pygame.K_DOWN] and self.rect.bottom + 5 <= Main.HEIGHT:  # Move down for arrow key control
                self.rect.y += 5

    def _got_hit(self, bullets):
        for bullet in bullets:
            if self.rect.colliderect(bullet.rect):
                print("hit " + self.control_type)
                bullets.remove(bullet)
                self._life.hp -= 1
                if self._life.hp == 0:
                    main.run = False
                    break

    def _update_bullets_pos(self, keys):
        current_time = pygame.time.get_ticks()

        if self.control_type == "wasd":
            for bullet in self.bullets:
                if bullet.rect.left > Main.WIDTH:
                    self.bullets.remove(bullet)
                else:
                    bullet.update_pos()
            # check if user shout new bullets
            if keys[pygame.K_LCTRL] and len(
                    self.bullets) < self.MAX_BULLETS and current_time - main.previous_time > 200:
                b = Bullet(self.rect.right, self.rect.y + self.rect.height / 2 - 2, "wasd")
                self.bullets.append(b)
                main.previous_time = current_time

        elif self.control_type == "arrows":
            for bullet in self.bullets:
                if bullet.rect.right < 0:
                    self.bullets.remove(bullet)
                else:
                    bullet.update_pos()

            # check if user shout new bullets
            if keys[pygame.K_RCTRL] and len(
                    self.bullets) < self.MAX_BULLETS and current_time - main.previous_time > 200:
                b = Bullet(self.rect.left - Bullet.BULLET_LENGTH, self.rect.y + self.rect.height / 2 - 2, "arrows")
                self.bullets.append(b)
                main.previous_time = current_time

    def draw(self, keys, enemy_bullets):
        self._got_hit(enemy_bullets)
        self._life.draw()

        self._update_bullets_pos(keys)
        self._update_player_pos(keys)

        # draw bullets
        for bullet in self.bullets:
            bullet.draw()

        # draw spaceships
        main.WIN.blit(self.surface, self.rect)


class HealthBar:
    def __init__(self, x, y, w, h, max_hp):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.hp = max_hp
        self.max_hp = max_hp

    def draw(self):
        # calculate health ratio
        ratio = self.hp / self.max_hp
        pygame.draw.rect(main.WIN, "red", (self.x, self.y, self.w, self.h))
        pygame.draw.rect(main.WIN, "green", (self.x, self.y, self.w * ratio, self.h))


if __name__ == "__main__":
    while True:
        main = Main()
        main.main()
        ans = input("Do you want to play again? ")
        if ans != "y":
            break
